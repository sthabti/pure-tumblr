Pure Tumblr
===========

Tumblr Theme using http://purecss.io and their example layout

Screen Shots

![Alt text](http://i.imgur.com/3nRVtnJ.png "Mobile")
![Alt text](http://i.imgur.com/79MIwPL.png "Standard")

Features
========

- Re-styled buttons
- Social media buttons - Can be edited and username names can be edit inside tumblr
- Description at the top
- Tumblr avatar next to each post
- Supports Tumlr Posts, Photos, Videos, Audio, Quotes, Audio
- Pages on the side
- Pure Csss is cool. 


Fork and do whatever you want. 
